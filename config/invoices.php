<?php

return [
    'footer_text'   => 'Invoice must be paid within 30 days',
    'logo_file'     => '/images/logo.png',
    'seller'        => [
        'name'      => 'Your company name',
        'address'   => 'dhaka Bangladesh',
        'email'     => '',
        'additional_info' => [
            'VAT Number' => 'XXXXXX'
        ]
    ]
];
