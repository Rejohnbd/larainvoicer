<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <title>PDF</title>
    <style>
        body {
            background: inherit;
            font-size: 14px
        }

        .tbl-total {
            width: inherit;
            border: 0;
        }

        .tbl-total th,
        .tbl-total tr,
        .tbl-total td {
            border: 0
        }
    </style>
</head>

<body>
    @yield('content')
</body>

</html>